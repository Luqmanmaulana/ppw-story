from django.shortcuts import render, redirect
from .forms import JadwalKegiatanForm
from .models import JadwalKegiatan

def jadwal(request):
    
    form = JadwalKegiatanForm(request.POST)
    # data = Jadwal_Kegiatan.objects.all()
    # context = {
    #     "formulir":form,
    #     "model":data
    # } 
    # if request.method == "POST" and form.is_valid():
    #     nama = form.cleaned_data['nama']
    #     tempat = form.cleaned_data['tempat']
    #     kategori = form.cleaned_data['kategori']
    #     hari = form.cleaned_data['hari']
    #     waktu = form.cleaned_data['waktu']

    #     model= Jadwal_Kegiatan(nama, tempat, kategori, hari, waktu)
    #     model.save()
    #     return render(request, 'jadwal.html', context)
    # return render(request, 'jadwal.html', {"formulir":form})


    if request.method == "POST" and form.is_valid():
        form.save()      
    return render(request, "jadwal.html", {"form": JadwalKegiatanForm()})

def lihat_jadwal(request):
    obj = JadwalKegiatan.objects.all()

    context = {
        'jadwals':obj
    }

    return render(request, 'lihat_jadwal.html', context)
def delete(request, delete_id):
    JadwalKegiatan.objects.filter(id=delete_id).delete()
    return redirect('/story5/lihat_jadwal')
