from django import forms
from .models import JadwalKegiatan
# from bootstrap_datepicker_plus import DateTimePickerInput, TimePickerInput

class JadwalKegiatanForm(forms.ModelForm): 
    nama = forms.CharField(widget=forms.TextInput(
        attrs = {
                'type' : 'text'
                }
    ))
    tempat = forms.CharField(widget=forms.TextInput(
        attrs = {
                'type' : 'text'
                }
    ))
    kategori = forms.CharField(widget=forms.TextInput(
        attrs = {
                'type' : 'text'
                }
    ))
    # hari = forms.CharField(label="", widget=forms.TextInput(
    #     attrs = {
    #             'type' : 'text'
    #             }
    # ))
    tanggal = forms.DateField(widget=forms.DateInput(
        attrs = {
                'type' : 'date'        
                }
    ))
    waktu = forms.TimeField(widget=forms.DateInput(
        attrs = {
                'type' : 'time'        
                }
    ))


    class Meta:
        model = JadwalKegiatan
        fields="__all__"
        # fields= [
        #     'nama',
        #     'tempat',
        #     'kategori',
        #     'tanggal',
        #     'waktu',
        # ]

        # labels = {
        #     'nama':'Nama',
        #     'tempat':'Tempat',
        #     'kategori':'Kategori',
        #     'tanggal':'Tanggal',
        #     'waktu':'Waktu',
        # }

        # widgets = {
        #     'name':forms.CharField(
        #         attrs={
                    
        #         },
        #     ),
        #     'tempat': forms.CharField(
        #         attrs={
                    
        #         },
        #     ),
        #     'kategori': forms.CharField(
        #         attrs={

        #         },
        #     ),
        #     'tanggal':forms.DateInput(
        #         attrs={
        #             'type' : 'date',
        #         },
        #     ),
        #     'waktu':forms.TimeInput(
        #         attrs={
        #             'type' : 'time',
        #         },
        #     ),
        # }

        