from django.urls import path
from .views import jadwal, lihat_jadwal, delete

app_name = "Story_5"

urlpatterns = [
    path('', jadwal, name="jadwal"),
    path('lihat_jadwal/', lihat_jadwal, name="lihat_jadwal"),
    path('jadwal/<int:delete_id>', delete, name='delete')
]