from django.db import models
from datetime import datetime

class JadwalKegiatan(models.Model):
    nama = models.CharField(max_length=30)
    tempat = models.CharField(max_length=20)
    kategori = models.CharField(max_length=20)
    # hari = models.CharField(max_length=10)
    tanggal = models.DateField()
    waktu = models.TimeField()

    def __str__(self):
        return self.nama