from django.shortcuts import render

def home(request):
    return render(request, "index.html")

def soon(request):
    return render(request, "soon.html")

def profile(request):
    return render(request, "profile.html")
    
def experiences(request):
    return render(request, "experiences.html")
    
def skills(request):
    return render(request, "skills.html")
